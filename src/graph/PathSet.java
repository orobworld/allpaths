package graph;

import java.util.TreeSet;

/**
 * PathSet organizes a collection of paths.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class PathSet {
  private final TreeSet<Path> paths;  // the collection of paths

  /**
   * Constructs an empty collection.
   */
  public PathSet() {
    paths = new TreeSet<>();
  }

  /**
   * Adds a path to the collection.
   * @param p the new path
   */
  public void add(final Path p) {
    paths.add(p);
  }

  /**
   * Clears out the path set.
   */
  public void clear() {
    paths.clear();
  }

  /**
   * Generates a summary of the path set.
   * @return a string summarizing the contents of the set
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    if (paths.isEmpty()) {
      return "There are no paths.";
    } else if (paths.size() == 1) {
      sb.append("There is one path.\n");
    } else {
      sb.append("There are ").append(paths.size()).append(" paths.\n");
    }
    for (Path p : paths) {
      sb.append(p).append("\n");
    }
    return sb.toString();
  }
}
