package graph;

/**
 * Arc represents an arc in the graph.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Arc implements Comparable<Arc> {
  private final int tail;   // source endpoint
  private final int head;   // sink endpoint

  /**
   * Constructor.
   * @param source the source node
   * @param sink the sink node
   */
  public Arc(final int source, final int sink) {
    tail = source;
    head = sink;
  }

  /**
   * Compares this arc to another one.
   * Arcs are ordered lexically by endpoints.
   * @param t the other arc
   * @return -1/0/1 if this are is earlier/equal/later in the ordering
   */
  @Override
  public int compareTo(final Arc t) {
    int z = Integer.compare(this.tail, t.getTail());
    if (z == 0) {
      z = Integer.compare(this.head, t.getHead());
    }
    return z;
  }

  /**
   * Provides a string representation of the edge.
   * @return a string representing the edge
   */
  @Override
  public String toString() {
    return "(" + tail + " -> " + head + ")";
  }

  /**
   * Finds the endpoint opposite to a given endpoint.
   * @param v the given endpoint
   * @return the opposite endpoint
   */
  public int oppositeTo(final int v) {
    if (v == tail) {
      return head;
    } else if (v == head) {
      return tail;
    } else {
      throw new IllegalArgumentException(v + " is not a vertex of the edge!");
    }
  }

  /**
   * Tests whether the edge is a loop (both endpoints the same).
   * @return true if the edge is a loop
   */
  public boolean isLoop() {
    return tail == head;
  }

  /**
   * Gets the tail (source) node.
   * @return the tail node
   */
  public int getTail() {
    return tail;
  }

  /**
   * Gets the head (sink) node.
   * @return the head node
   */
  public int getHead() {
    return head;
  }

}
