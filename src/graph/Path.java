package graph;

import java.util.ArrayList;

/**
 * Path holds a path through the graph.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Path implements Comparable<Path> {
  private final int length;           // number of arcs
  private final ArrayList<Arc> arcs;  // sequence of arcs
  private final int endpoint;         // final vertex

  /**
   * Constructs a path from a single arc.
   * @param a the lone arc in the path
   */
  public Path(final Arc a) {
    length = 1;
    arcs = new ArrayList<>();
    arcs.add(a);
    endpoint = a.getHead();
  }

  /**
   * Extends an existing path.
   * @param p the existing path
   * @param a the arc to append
   */
  public Path(final Path p, final Arc a) {
    // Make sure the extension is valid.
    if (a.getTail() != p.getTerminus()) {
      throw new IllegalArgumentException("Cannot extend a path ending at "
                                         + p.getTerminus()
                                         + " with an arc starting at "
                                         + a.getTail() + "!");
    }
    // Extend the previous path.
    endpoint = a.getHead();
    length = p.getLength() + 1;
    arcs = p.getArcs();
    arcs.add(a);
  }

  /**
   * Compares this path to another path.
   * Paths are ordered by ascending length. Within the same length, the
   * ordering is lexicographic based on the arcs.
   * @param other the other path
   * @return -1/0/1 if this path comes earlier/equal/later in the ordering
   */
  @Override
  public int compareTo(final Path other) {
    int z = Integer.compare(this.length, other.getLength());
    if (z == 0) {
      // Equal length: break the tie lexically by arcs.
      ArrayList<Arc> olist = other.getArcs();
      for (int i = 0; i < length; i++) {
        z = arcs.get(i).compareTo(olist.get(i));
        if (z != 0) {
          return z;
        }
      }
    }
    return z;
  }

  /**
   * Gets the path length.
   * @return the path length
   */
  public int getLength() {
    return length;
  }

  /**
   * Gets the final node in the path.
   * @return the final node
   */
  public int getTerminus() {
    return endpoint;
  }

  /**
   * Gets a copy of the list of arcs in the path.
   * @return the list of arcs
   */
  public ArrayList<Arc> getArcs() {
    return new ArrayList<>(arcs);
  }

  /**
   * Gets a string representation of the path.
   * @return a string describing the path
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (Arc a : arcs) {
      sb.append(a.getTail()).append(" -> ");
    }
    sb.append(endpoint).append(" (length = ").append(length).append(")");
    return sb.toString();
  }
}
