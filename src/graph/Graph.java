package graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Graph holds a graph instance.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Graph {
  private final HashMap<Integer, Set<Arc>> outbound;   // maps vertices to
                                                       // arcs exiting there
  private final HashMap<Integer, Set<Arc>> inbound;    // maps vertices to
                                                       // arcs entering there
  private final HashSet<Arc> arcs;                     // all arcs

  /**
   * Constructs a graph from an array of arcs.
   * @param a an nx2 matrix of arcs
   */
  public Graph(final int[][] a) {
    outbound = new HashMap<>();
    inbound = new HashMap<>();
    arcs = new HashSet<>();
    for (int[] e : a) {
      addArc(e[0], e[1]);
    }
  }

  /**
   * Adds an arc to the graph.
   * @param v1 one tail of the arc
   * @param v2 the head of the arc
   */
  public void addArc(final int v1, final int v2) {
    Arc a = new Arc(v1, v2);
    arcs.add(a);
    if (outbound.containsKey(v1)) {
      outbound.get(v1).add(a);
    } else {
      HashSet<Arc> s = new HashSet<>();
      s.add(a);
      outbound.put(v1, s);
    }
    if (inbound.containsKey(v2)) {
      inbound.get(v2).add(a);
    } else {
      HashSet<Arc> s = new HashSet<>();
      s.add(a);
      inbound.put(v2, s);
    }
  }

  /**
   * Constructs a string summary of the graph.
   * @return a summary of the graph
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("The graph contains ").append(outbound.keySet().size())
      .append(" vertices and ").append(arcs.size()).append(" arcs.");
    int n = (int) arcs.stream().filter((a -> a.isLoop())).count();
    switch (n) {
      case 0:
        sb.append(" There are no self-loops.");
        break;
      case 1:
        sb.append(" There is one self-loop.");
        break;
      default:
        sb.append(" There are ").append(n).append(" self-loops.");
    }
    return sb.toString();
  }

  /**
   * Gets the set of arcs exiting a vertex.
   * @param v the vertex
   * @return the arcs exiting there
   */
  public Set<Arc> exiting(final int v) {
    if (outbound.containsKey(v)) {
      return new HashSet<>(outbound.get(v));
    } else {
      return new HashSet<>();
    }
  }

  /**
   * Gets the set of arcs entering a vertex.
   * @param v the vertex
   * @return the arcs entering there
   */
  public Set<Arc> entering(final int v) {
    if (inbound.containsKey(v)) {
      return new HashSet<>(inbound.get(v));
    } else {
      return new HashSet<>();
    }
  }

  /**
   * Returns the set of all arcs.
   * @return a copy of the arc set
   */
  public Set<Arc> getAllArcs() {
    return new HashSet<>(arcs);
  }
}
