package solver;

import graph.Arc;
import graph.Graph;
import graph.Path;
import graph.PathSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * Enumeration solves the problem by brute force (enumerating all suitable
 * paths by construction).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Enumeration {
  private final Graph graph;            // the graph
  private final PathSet paths;          // all suitable paths found
  private int destination;              // the target node for all paths
  private int maxLength;                // the maximum path length

  /**
   * Constructor.
   * @param g the graph to use
   */
  public Enumeration(final Graph g) {
    graph = g;
    paths = new PathSet();
  }

  /**
   * Enumerates all paths from a designated source to a designated sink of
   * length less than a specified cutoff and with no arcs repeated.
   * @param from the source node
   * @param to the sink node
   * @param maxL the maximum number of arcs in a path
   * @return the collection of paths found
   */
  public PathSet solve(final int from, final int to, final int maxL) {
    long time = System.currentTimeMillis();
    paths.clear();
    destination = to;
    maxLength = maxL;
    // Create a FIFO queue of paths to extend.
    Queue<Path> todo = new LinkedList<>();
    // Turn every arc exiting the source node into a path and queue it.
    for (Arc a : graph.exiting(from)) {
      Path p = new Path(a);
      check(p);
      todo.add(p);
    }
    // Try all possible extensions of each incomplete path.
    while (!todo.isEmpty()) {
      // Get the next path to extend
      Path p = todo.poll();
      // Find all arcs exiting its terminus and not already on the path.
      Set<Arc> s = graph.exiting(p.getTerminus());
      s.removeAll(p.getArcs());
      for (Arc a : s) {
        // Extend the path using arc a.
        Path q = new Path(p, a);
        // Check whether it is a solution.
        check(q);
        // If is not already maximal length, queue it for extension.
        if (q.getLength() < maxLength) {
          todo.add(q);
        }
      }
    }
    System.out.println("Solution time = "
                       + (System.currentTimeMillis() - time)
                       + " ms.");
    return paths;
  }

  /**
   * Checks a path to see if it qualifies as a solution and, if so, records it.
   * @param p the path to check
   */
  private void check(final Path p) {
    if (p.getTerminus() == destination && p.getLength() <= maxLength) {
      paths.add(p);
    }
  }

}
