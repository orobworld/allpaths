package solver;

import graph.Arc;
import graph.Graph;
import graph.Path;
import graph.PathSet;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

/**
 * MIP implements a mixed integer programming model for the path problem.
 *
 * The model uses binary variables to determine the sequence of arcs used.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MIP implements AutoCloseable {
  private static final double HALF = 0.5;       // for rounding MIP solutions
  private final IloCplex mip;  // the MIP model
  private final ArrayList<IloIntVar> intVars;   // master list of binary vars
  private final BidiMap<Arc, IloIntVar> used;   // indicator the arc is used
  private final BidiMap<Arc, IloIntVar> first;  // indicator for first arc
                                                // in path
  private final BidiMap<Arc, IloIntVar> last;   // indicator for last arc
                                                // in path
  private final HashMap<Arc, HashMap<Arc, IloIntVar>> follows;
                                                // indicator that one arc
                                                // follows another
  private final HashMap<IloIntVar, Arc[]> following;
                                                // maps following indicator to
                                                // first and second arcs
  private final BidiMap<Arc, IloNumVar> sequence;  // MTZ sequence variables

  /**
   * Constructor.
   * @param g the graph to solve
   * @param source the origin node
   * @param sink the destination node
   * @param limit the maximum number of arcs in a path
   * @throws IloException if the MIP model cannot be constructed.
   */
  public MIP(final Graph g, final int source, final int sink, final int limit)
         throws IloException {
    long time = System.currentTimeMillis();
    // Initialize storage fields.
    intVars = new ArrayList<>();
    used = new DualHashBidiMap<>();
    first = new DualHashBidiMap<>();
    last = new DualHashBidiMap<>();
    follows = new HashMap<>();
    following = new HashMap<>();
    sequence = new DualHashBidiMap<>();
    // Get the set of all arcs.
    Set<Arc> arcs = g.getAllArcs();
    // Initialize the model.
    mip = new IloCplex();
    // Create the variables.
    for (Arc a : arcs) {
      // Create a variable for whether the arc is used.
      IloIntVar x = mip.boolVar(label("used", a));
      intVars.add(x);
      used.put(a, x);
      // Create a variable for the sequence value (0 if first or not used,
      // 1 ... M if used and not first) of the arc.
      IloNumVar seq = mip.numVar(0, limit, label("sequence", a));
      sequence.put(a, seq);
      // Check for a possible first arc.
      if (a.getTail() == source) {
        x = mip.boolVar(label("first", a));
        intVars.add(x);
        first.put(a, x);
      }
      // Check for a possible last arc.
      if (a.getHead() == sink) {
        x = mip.boolVar(label("last", a));
        intVars.add(x);
        last.put(a, x);
      }
      // Look for arcs that can chain with this one.
      for (Arc b : g.exiting(a.getHead())) {
        // Skip self-loops.
        if (b == a) {
          continue;
        }
        x = mip.boolVar(label2("following", a, b));
        intVars.add(x);
        following.put(x, new Arc[] {a, b});
        // Insert it into the `follows` map.
        insert(a, b, x);
      }
    }
    // Constraints begin here.
    // There must be one first arc and one last arc.
    IloIntVar[] z = first.values().toArray(IloIntVar[]::new);
    mip.addEq(mip.sum(z), 1, "single_first_arc");
    z = last.values().toArray(IloIntVar[]::new);
    mip.addEq(mip.sum(z), 1, "single_last_arc");
    // There is a limit on the number of arcs used.
    z = used.values().toArray(IloIntVar[]::new);
    mip.addLe(mip.sum(z), limit, "max_arcs_used");
    for (Arc a : arcs) {
      // An arc is used if and only if it is either the first arc or it follows
      // another arc. Note that this will implicitly limit any arc to being used
      // once.
      IloLinearNumExpr expr = mip.linearNumExpr();
      expr.addTerm(-1, used.get(a));
      if (first.containsKey(a)) {
        expr.addTerm(1, first.get(a));
      }
      for (Arc b : arcs) {
        HashMap<Arc, IloIntVar> m = follows.get(b);
        if (m != null && m.containsKey(a)) {
          expr.addTerm(1, m.get(a));
        }
      }
      mip.addEq(expr, 0, label("use", a));
      // The last arc must be used.
      if (last.containsKey(a)) {
        mip.addLe(last.get(a), used.get(a), label("last_used", a));
      }
      // The sequence value of an arc is 0 if it is not used.
      mip.addLe(sequence.get(a), mip.prod(limit, used.get(a)),
                label("unused_seq", a));
      // No arc can follow the last arc.
      if (last.containsKey(a) && follows.containsKey(a)
          && !follows.get(a).isEmpty()) {
        expr = mip.linearNumExpr();
        expr.addTerm(1, last.get(a));
        for (IloIntVar v : follows.get(a).values()) {
          expr.addTerm(1, v);
        }
        mip.addLe(expr, 1, label("cannot_follow_last", a));
      }
      // If an arc is used, either it is the last arc or some arc follows it.
      expr = mip.linearNumExpr();
      expr.addTerm(-1, used.get(a));
      if (last.containsKey(a)) {
        expr.addTerm(1, last.get(a));
      }
      if (follows.containsKey(a) && !follows.get(a).isEmpty()) {
        for (IloIntVar v : follows.get(a).values()) {
          expr.addTerm(1, v);
        }
      }
      mip.addEq(expr, 0, label("end_or_be_followed", a));
      // If an arc follows a, its sequence number must be one higher than
      // that of a.
      if (follows.containsKey(a)) {
        HashMap<Arc, IloIntVar> map = follows.get(a);
        if (map != null && !map.isEmpty()) {
          IloNumVar sa = sequence.get(a);
          for (Arc b : map.keySet()) {
            expr = mip.linearNumExpr();
            expr.addTerm(1, sa);
            expr.addTerm(-1, sequence.get(b));
            expr.addTerm(limit + 1, map.get(b));
            mip.addLe(expr, limit, label2("MTZ", a, b));
          }
        }
      }
    }
    // The objective function defaults to minimizing zero.
    System.out.println("MIP construction took "
                       + (System.currentTimeMillis() - time) + " ms.");
  }

  /**
   * Closes the problem instance.
   */
  @Override
  public void close() {
    mip.end();
  }

  /**
   * Creates a CPLEX-friendly variable label based on a prefix string and
   * an arc.
   * @param prefix the prefix
   * @param a the arc
   * @return the label
   */
  private String label(final String prefix, final Arc a) {
    return prefix + "_" + a.getTail() + "_" + a.getHead();
  }

  /**
   * Creates a CPLEX-friendly variable label based on a prefix string and
   * two arcs.
   * @param prefix the prefix
   * @param a the first arc
   * @param b the second arc
   * @return the label
   */
  private String label2(final String prefix, final Arc a, final Arc b) {
    return label(prefix, a) + "_" + b.getTail() + "_" + b.getHead();
  }

  /**
   * Inserts a variable into the `follows` map.
   * @param a the first arc
   * @param b the second arc
   * @param x the indicator variable that the second arc follows the first one
   */
  private void insert(final Arc a, final Arc b, final IloIntVar x) {
    HashMap<Arc, IloIntVar> m;
    if (follows.containsKey(a)) {
      m = follows.get(a);
    } else {
      m = new HashMap<>();
      follows.put(a, m);
    }
    m.put(b, x);
  }

  /**
   * Searches for feasible solutions.
   * @param timeLimit the time limit (in seconds)
   * @param intensity the search intensity setting
   * @param capacity the maximum number of solutions to find
   * @return true if at least one solution is found
   * @throws IloException if CPLEX pitches a fit
   */
  public boolean populate(final double timeLimit, final int intensity,
                          final int capacity) throws IloException {
    mip.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
    mip.setParam(IloCplex.Param.MIP.Pool.Intensity, intensity);
    mip.setParam(IloCplex.Param.MIP.Pool.Capacity, capacity);
    return mip.populate();
  }

  /**
   * Gets the number of solutions found.
   * @return the number of solutions found
   * @throws IloException if CPLEX throws an error
   */
  public int getSolutionCount() throws IloException {
    return mip.getSolnPoolNsolns();
  }

  /**
   * Gets the solutions.
   * @return the set of all paths found
   * @throws IloException if CPLEX throws an error
   */
  public PathSet getSolutions() throws IloException {
    PathSet pset = new PathSet();
    IloNumVar[] vars = intVars.toArray(IloNumVar[]::new);
    for (int n = 0; n < mip.getSolnPoolNsolns(); n++) {
      // Get solution n.
      double[] x = mip.getValues(vars, n);
      // Identify "true" variables.
      HashSet<IloNumVar> sol = new HashSet<>();
      for (int i = 0; i < vars.length; i++) {
        if (x[i] > HALF) {
          sol.add(vars[i]);
        }
      }
      // Get the values of the sequence variables.
      HashMap<Double, Arc> seq = new HashMap<>();
      for (Arc a : sequence.keySet()) {
        double z = mip.getValue(sequence.get(a), n);
        if (z > HALF) {
          seq.put(z, a);
        }
      }
      // Turn it into a path and add it to the set.
      pset.add(makePath(sol, seq));
    }
    return pset;
  }

  /**
   * Turns a solution into a path.
   * @param sol the solution (set of variables that take value "true")
   * @param seq values of the sequence variables
   * @return the path represented by the solution
   */
  private Path makePath(final HashSet<IloNumVar> sol,
                        final Map<Double, Arc> seq) {
    Path p = null;
    Arc a = null;
    // Find the first arc.
    for (IloIntVar v : first.values()) {
      if (sol.contains(v)) {
        a = first.getKey(v);
        p = new Path(a);
        break;
      }
    }
    // Use the MTZ sequence values to complete the path.
    List<Double> keys = seq.keySet().stream().sorted().toList();
    for (double k : keys) {
      p = new Path(p, seq.get(k));
    }
    return p;
  }
}
