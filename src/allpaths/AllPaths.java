package allpaths;

import graph.Graph;
import graph.PathSet;
import ilog.concert.IloException;
import solver.Enumeration;
import solver.MIP;

/**
 * AllPaths computes all paths of length less than a specified limit between
 * two nodes in a directed graph.
 *
 * Nodes can be visited more than once, but arcs can be used at most once in
 * any path. This is based on a post by Erwin Kalvelagen:
 *
 * https://yetanothermathprogrammingconsultant.blogspot.com/2022/01/an-all
 * -paths-network-problem.html
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class AllPaths {

  /**
   * Dummy constructor.
  */
  private AllPaths() { }

  /**
   * Runs the computational experiment.
   *
   * @param args the command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Create an array of edges present in Erwin's example.
    int[][] arcs = new int[][] {{1, 2}, {1, 4}, {1, 8},
                                {2, 6}, {2, 8},
                                {3, 3}, {3, 8}, {3, 9},
                                {4, 3}, {4, 4}, {4, 6}, {4, 7},
                                {5, 9},
                                {6, 5}, {6, 10},
                                {7, 1}, {7, 5},
                                {8, 3}, {8, 10},
                                {9, 1}, {9, 7}, {9, 10}};
    // Create the graph instance.
    Graph g = new Graph(arcs);
    System.out.println(g);
    // Designate the origin and destination for the target paths.
    int origin = 1;
    int destination = 10;
    // Set the limit on the length of a path.
    int maxLength = 4;
    // Try solving the problem by brute force.
    Enumeration solver = new Enumeration(g);
    PathSet paths = solver.solve(origin, destination, maxLength);
    System.out.println(paths);
    // Try solving using the MIP model and solution pool.
    System.out.println("\nTrying the MIP model ...");
    try (MIP mip = new MIP(g, origin, destination, maxLength)) {
      double timeLimit = 120;  // time limit in seconds
      int intensity = 4;       // search intensity (maximal)
      int solLimit = 20;       // stop after this many solutions
      if (mip.populate(timeLimit, intensity, solLimit)) {
        PathSet pset = mip.getSolutions();
        System.out.println(pset);
      }
    } catch (IloException ex) {
      System.out.println("The MIP blew up:\n" + ex.getMessage());
    }
  }
}
