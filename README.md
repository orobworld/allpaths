# All Paths #

### What is this repository for? ###

A question posted on Stack Overflow (and subsequently deleted) led to a [blog post](https://yetanothermathprogrammingconsultant.blogspot.com/2022/01/an-all-paths-network-problem.html) by Erwin Kalvelagen on how to find all paths between two nodes in a directed graph (possibly with self-loops, i.e. arcs from a node to itself) subject to two constraints: no arc can be used more than once in a path; and there is an upper limit *M* on the number of arcs used. Note that a path might visit a *node* more than once. It just cannot repeat an arc.

Erwin explored some mixed integer linear programming (MIP) models in his post, and a [subsequent post](https://or.stackexchange.com/questions/7966/how-to-compute-all-paths-between-two-given-nodes-in-a-network/) on OR Stack Exchange led to more proposals of MIP models (including one from me). I also suggested that a "brute force" approach might be faster than any of the MIP models.

This code incorporates both the brute force approach and the MIP model I proposed. It requires both the open-source Apache Commons Collections library (version 4.4 or later) and CPLEX. The code was developed using a beta version of CPLEX Studio 22.1 but will run with at least some earlier versions. A full description of both approaches is in a [post](https://orinanobworld.blogspot.com/2022/03/finding-almost-all-paths.html) on my blog.

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

